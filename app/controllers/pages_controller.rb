class PagesController < ApplicationController
	def home
		
	end
	def callprocessing
		pid = fork do
			Headless.ly do
				subpid = Process.spawn("/usr/share/processing/processing-java --run --force --sketch=public/scripts/sample --output=public/outputs/sample",:pgroup => true)
				sleep(10)
				Process.detach(subpid)
				Process.kill(-9,subpid)		
				exit
			end
		end
		Process.detach(pid)		
				
	end
end
