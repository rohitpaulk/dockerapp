void setup(){	
	size(500,500,P3D);
	background(255);
	rectMode(CENTER);
	fill(210);
	noStroke();
	noLoop();
}

void draw(){
	int tubes = 6;
	int innerradius = 35;
	int outerradius = 45;
	int height = 200;
	int sides = 40;
	lights();
	directionalLight(80,80,80,0,1,0);
	ortho();
	translate(250,250,0);
	rotateX(radians(60));
	rotateY(radians(0));
 	rotateZ(radians(45));
	for (int i = -200; i < 200; i++) {
		point(0,0,10*i);
		point(0,10*i,0);
		point(10*i,0,0);
  	}
 	
	float[] Xpositions = new float[7]; // tubes-1
	float[] Ypositions = new float[7];
	
	Xpositions[0] = 0.0;
	Ypositions[0] = 0.0;

	Xpositions[1] = 2*outerradius;
	Ypositions[1] = 0.0;

	Xpositions[2] = -2*outerradius;
	Ypositions[2] = 0.0;

	Xpositions[3] = 2*outerradius*cos(radians(60));
	Ypositions[3] = 2*outerradius*sin(radians(60));

	Xpositions[4] = -2*outerradius*cos(radians(60));
	Ypositions[4] = 2*outerradius*sin(radians(60));

	Xpositions[5] = 2*outerradius*cos(radians(60));
	Ypositions[5] = -2*outerradius*sin(radians(60));

	Xpositions[6] = -2*outerradius*cos(radians(60));
	Ypositions[6] = -2*outerradius*sin(radians(60));

	for (int i = 0; i < 7; i++) {
		pushMatrix();
	 		translate(Xpositions[i],Ypositions[i],0);
	 		drawHollowCylinder(sides,innerradius,outerradius,height);	
	    popMatrix();	
  	}	
  	
	save("sample.png");
}


void drawHollowCylinder(int sides, float r1, float r2,  float h)
{
	// r1 - Inner Radius
	// r2 - Outer Radius
	float angle = 360 / sides;
	float halfHeight = h / 2;
	// draw top shape
	beginShape();

	// Draw the top outer one
	for (int i = 0; i < sides; i++) {
		float x = cos( radians( i * angle ) ) * r2;
		float y = sin( radians( i * angle ) ) * r2;
		vertex( x, y, -halfHeight );    
	}
	// Cut out inner shape
	beginContour();
	for (int i = 0; i < sides; i++) {
		float x = cos( radians( i * angle ) ) * r1;
		float y = sin( radians( i * angle ) ) * r1;
		vertex( x, y, -halfHeight );    
	}
	endContour();
	endShape(CLOSE);
	// draw bottom shape
	beginShape();
	for (int i = 0; i < sides; i++) {
		float x = cos( radians( i * angle ) ) * r2;
		float y = sin( radians( i * angle ) ) * r2;
		vertex( x, y, halfHeight );    
	}
	// Cut out Inner shape
	beginContour();
	for (int i = 0; i < sides; i++) {
		float x = cos( radians( i * angle ) ) * r1;
		float y = sin( radians( i * angle ) ) * r1;
		vertex( x, y, halfHeight );    
	}
	endContour();
	endShape(CLOSE);
	noStroke();

	beginShape(TRIANGLE_STRIP);
	for (int i = 0; i < sides + 1; i++) {
		float x = cos( radians( i * angle ) ) * r1;
		float y = sin( radians( i * angle ) ) * r1;
		vertex( x, y, halfHeight);
		vertex( x, y, -halfHeight);    
	}
	endShape(CLOSE);
	beginShape(TRIANGLE_STRIP);
	for (int i = 0; i < sides + 1; i++) {
		float x = cos( radians( i * angle ) ) * r2;
		float y = sin( radians( i * angle ) ) * r2;
		vertex( x, y, halfHeight);
		vertex( x, y, -halfHeight);    
	}
	endShape(CLOSE);
	fill(180);
	pushMatrix();
		translate(0,0,-halfHeight);
		int hue = 200;
		for (int r = int(r1); r > 0; --r) {
    		fill(hue);
    		ellipse(0, 0, 2*r, 2*r);
    		hue = (hue + (60/r));
    		
  		}
	popMatrix();
	fill(210);
}
