# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Dockerapp::Application.config.secret_key_base = '4f4232a2bf44e1ce2b1a0df1c5faff090c071012fc297c9beda9a57ba811ab2116f82209141f9ccea85db7dbf757f825e845e32aae14e42c93aca0a82945130d'
