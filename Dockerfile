# Select ubuntu as the base image
FROM ubuntu:14.04

# Install nginx, nodejs and curl
RUN apt-get update -q
RUN apt-get install -qy nginx
RUN apt-get install -qy curl
RUN apt-get install -qy nodejs
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Install rvm, ruby, bundler
RUN curl -sSL https://get.rvm.io | bash -s stable
RUN /bin/bash -l -c "rvm requirements"
RUN /bin/bash -l -c "rvm install 2.1.2"
RUN /bin/bash -l -c "gem install bundler --no-ri --no-rdoc"

# Install processing
ADD http://download.processing.org/processing-2.2.1-linux64.tgz processing-2.2.1-linux64.tgz
RUN tar -xf processing-2.2.1-linux64.tgz
RUN rm processing-2.2.1-linux64.tgz
RUN mv processing-2.2.1 /usr/share/processing

# Add configuration files in repository to filesystem
ADD https://bitbucket.org/rohitpaulk/dockerapp/raw/master/config/container/nginx-sites.conf /etc/nginx/sites-enabled/default
ADD https://bitbucket.org/rohitpaulk/dockerapp/raw/master/config/container/start-server.sh /usr/bin/start-server
RUN chmod +x /usr/bin/start-server

# Cache the bundle install if the Gemfile hasn't changed
ADD https://bitbucket.org/rohitpaulk/dockerapp/raw/master/Gemfile /rails/Gemfile
ADD https://bitbucket.org/rohitpaulk/dockerapp/raw/master/Gemfile.lock /rails/Gemfile.lock
# should have been WORKDIR /rails
RUN cd /rails && /bin/bash -l -c "bundle install" 

# Add rails project to project directory

RUN apt-get install -qy git
RUN rm -rf /rails
ADD https://bitbucket.org/rohitpaulk/dockerapp/get/master.tar.gz repo.tar.gz
RUN tar -xf repo.tar.gz
RUN rm repo.tar.gz
RUN mv `ls | grep rohitpaulk-dockerapp` /rails

RUN apt-get install -qy xvfb
RUN apt-get install -qy libxp6 libxp-dev
RUN apt-get install -qy libxrender1 libxtst6 libxext6 libxi6


# Publish port 80
EXPOSE 80

# # Startup commands
# # RUN /bin/bash -l -c "/usr/bin/start-server"

# RUN export DISPLAY=:99
# RUN /etc/init.d/xvfb start